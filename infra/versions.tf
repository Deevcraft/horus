terraform {
  required_version = "~> 1.7.2"

  backend "remote" {
    organization = "Devcraft"

    workspaces {
      name = "horus"
    }
  }

 required_providers {
    oci = {
      source  = "oracle/oci"
      version = "5.27.0"
    }
  }
}




provider "oci" {
  auth = "SecurityToken"
  config_file_profile = "PROFILE"
  region = "eu-marseille-1"
}



  
