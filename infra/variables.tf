variable "tenancy_id" {
  description = "The ID of the tenancy (same with the root compartment ID)"
  type        = string
}

variable "compartment_name" {
  description = "Name of the compartment where to create all resources"
  type        = string
  default     = "horus"
}

variable "compartment_description" {
  description = "Description of the compartment where to create all resources"
  type        = string
  default     = "Horus Project"
}

variable "api_fingerprint" {
  description = "Description of the fingerprint  where to create all resources"
  type        = string
  default     = "7e:ba:f1:01:b2:c5:4a:26:5e:56:4e:d1:65:53:6e:3b"
}

variable "api_private_key_path" {
  description = "Description of the key path where to create all resources"
  type        = string
  default     = "/home/yoda/.oci/private.pem"
}

variable "user_id" {
  description = "Description of the user id  where to create all resources"
  type        = string
  default     = "ocid1.user.oc1..aaaaaaaaayitkp5epaaozmao2liup22pucx43qfvdeetsdmvymnn5i5fi2hq"
}


variable "region" {
  description = "Description of the region where to create all resources"
  type        = string
  default     = "eu-marseille-1"
}


